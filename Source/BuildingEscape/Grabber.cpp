// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Components/PrimitiveComponent.h"

#define OUT


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}



// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	FindPhysicHandleController();
	SetupInputComponent();
	

	// ...
	
}

void UGrabber::FindPhysicHandleController() {
	PhysicHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	 if (PhysicHandle==nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Physic Handle Component Not Found at %s object."), *GetOwner()->GetName());
	}
}

void UGrabber::SetupInputComponent()
{
	PlayerInputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (PlayerInputComponent) {
		PlayerInputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		PlayerInputComponent->BindAction("Release", IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("PlayerInputComponent Not Found at %s objects"), *GetOwner()->GetName());
	}
}



void UGrabber::Release() {
	if (!PhysicHandle) { return; }
	PhysicHandle->ReleaseComponent();
}

void UGrabber::Grab() {
	auto HitResult = GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();

	if (ActorHit){ 
		if (!PhysicHandle) { return; }
	PhysicHandle->GrabComponent(
	  ComponentToGrab, NAME_None, ComponentToGrab->GetOwner()->GetActorLocation(), true);
	}
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!PhysicHandle) { return; }
	if (PhysicHandle->GrabbedComponent) {
		PhysicHandle->SetTargetLocation(GetReachLineEnd());
	}

}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() 
{
	FHitResult HitResult;
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());

/*	FVector DebugLineLenght = FVector(0.0f, 0.0f, 50.0f);
	FVector LineTraceEndVector = PlayerViewPointLocation + DebugLineLenght;
	
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);
	FVector LineTraceEnd = PlayerViewPointLocation + GetWorld()->GetFirstPlayerController()->GetActorForwardVector()*Reach;
	
	PlayerForwardVector = GetWorld()->GetFirstPlayerController()->GetActorForwardVector();

	if (PhysicHandle->GrabbedComponent) {
		PhysicHandle->SetTargetLocation(LineTraceEnd);
	}
	*/

	GetWorld()->LineTraceSingleByObjectType(
		OUT HitResult,
		GetReachLineStart(),
		GetReachLineEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParameters

	);
	//AActor* HitAct = HitResult.GetActor();

	return HitResult;
}

FVector UGrabber::GetReachLineStart() {
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);
	return PlayerViewPointLocation;
}

FVector UGrabber::GetReachLineEnd() {
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);
	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;
}

