// Copyright SPDPoland 2017

#include "OpenDoor.h"
#include "BuildingEscape.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Engine/World.h"
#include "Containers/Array.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	//ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	Owner = GetOwner();
	if (!PressurePlate) {
		UE_LOG(LogTemp, Error, TEXT( "No PressurePlate set at %s object"), *GetOwner()->GetName());
	}
	//OpenDoor();
	
	
}


void UOpenDoor::OpenDoor()
{

//	Owner->SetActorRotation(FRotator(0.0f, OpenAngle, 0.0f));
	OnOpen.Broadcast();
}

void UOpenDoor::CloseDoor()
{
	//Owner->SetActorRotation(FRotator(0.0f, 180.0f, 0.0f));
	OnClose.Broadcast();
	
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


	// ...
	if (GetTotalMassOfActorOnPlate()>OpenWeigth) 
	{
		OpenDoor();
		//LastDoorOpenTime = GetWorld()->GetTimeSeconds();
	}
	else{
		CloseDoor();
	}
	
}

float UOpenDoor::GetTotalMassOfActorOnPlate(){
	float TotalMass = 0.0f;
	TArray <AActor*> OverlappingActors;
	if (!PressurePlate) { return 0; }
		PressurePlate->GetOverlappingActors(OUT OverlappingActors);
	//int32 Numnum = OverlappingActors.;
	//if (Numnum > 0) {
	//	UE_LOG(LogTemp, Warning, TEXT("%s"), OverlappingActors.Num());
	//}
	for (const auto* Actor : OverlappingActors) {
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Actor->GetName());
		//UPrimitiveComponent tempPrim = Cast<UPrimitiveComponent>(str->FindComponentByClass(UPrimitiveComponent::StaticClass());
	//	if (Actor->FindComponentByClass<UPrimitiveComponent>()) {
		TotalMass = TotalMass+Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	//	UE_LOG(LogTemp, Warning, TEXT("KG %s"), TotalMass);
//	}
	}
	return TotalMass;
}

